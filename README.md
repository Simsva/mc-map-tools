# Minecraft Map Tools

A Python (>=3.11) library to help with designing Minecraft map art.
NOTE: most functions are horribly inefficient, but work.

## Usage

Import the library (`mcmap.py`) in a script or the Python interpreter. Some
example scripts are provided.

A list of possible colors can be found in `mcmap.complete_colors`. For
information on which blocks produce which colors, see the [Minecraft
Wiki](https://minecraft.fandom.com/wiki/Map_item_format#Base_colors).
