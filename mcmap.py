#!/usr/bin/env python3

import sys
assert sys.version_info >= (3, 11)

from enum import IntEnum
from typing import NamedTuple, Self, TypeVar, Callable
from functools import reduce

RGB = tuple[int, int, int]
RGBA = tuple[int, int, int, int]
RGBADist = Callable[[RGBA | RGB, RGBA | RGB], float]

class BaseColor(IntEnum):
    """Basic map color, every entry corresponds to four shades."""

    NONE = 0
    GRASS = 1
    SAND = 2
    WOOL = 3
    FIRE = 4
    ICE = 5
    METAL = 6
    PLANT = 7
    SNOW = 8
    CLAY = 9
    DIRT = 10
    STONE = 11
    WATER = 12
    WOOD = 13
    QUARTZ = 14
    COLOR_ORANGE = 15
    COLOR_MAGENTA = 16
    COLOR_LIGHT_BLUE = 17
    COLOR_YELLOW = 18
    COLOR_LIGHT_GREEN = 19
    COLOR_PINK = 20
    COLOR_GRAY = 21
    COLOR_LIGHT_GRAY = 22
    COLOR_CYAN = 23
    COLOR_PURPLE = 24
    COLOR_BLUE = 25
    COLOR_BROWN = 26
    COLOR_GREEN = 27
    COLOR_RED = 28
    COLOR_BLACK = 29
    GOLD = 30
    DIAMOND = 31
    LAPIS = 32
    EMERALD = 33
    PODZOL = 34
    NETHER = 35
    TERRACOTTA_WHITE = 36
    TERRACOTTA_ORANGE = 37
    TERRACOTTA_MAGENTA = 38
    TERRACOTTA_LIGHT_BLUE = 39
    TERRACOTTA_YELLOW = 40
    TERRACOTTA_LIGHT_GREEN = 41
    TERRACOTTA_PINK = 42
    TERRACOTTA_GRAY = 43
    TERRACOTTA_LIGHT_GRAY = 44
    TERRACOTTA_CYAN = 45
    TERRACOTTA_PURPLE = 46
    TERRACOTTA_BLUE = 47
    TERRACOTTA_BROWN = 48
    TERRACOTTA_GREEN = 49
    TERRACOTTA_RED = 50
    TERRACOTTA_BLACK = 51
    CRIMSON_NYLIUM = 52
    CRIMSON_STEM = 53
    CRIMSON_HYPHAE = 54
    WARPED_NYLIUM = 55
    WARPED_STEM = 56
    WARPED_HYPHAE = 57
    WARPED_WART_BLOCK = 58
    DEEPSLATE = 59
    RAW_IRON = 60
    GLOW_LICHEN = 61

    def __str__(self) -> str:
        return f"{self.name}"

class ColorShade(IntEnum):
    """Shade specifier for a 'BaseColor'."""

    SLOPE_NS = 0
    PLANE    = 1
    SLOPE_SN = 2
    BASE     = 2
    DARK     = 3

    def __str__(self) -> str:
        return f"{self.name}"

shade_multipliers: dict[ColorShade, int] = {
    ColorShade.SLOPE_NS: 180,
    ColorShade.PLANE:    220,
    ColorShade.SLOPE_SN: 255,
    ColorShade.DARK:     135,
}

def color_dist_euclidian(a: RGBA | RGB, b: RGBA | RGB) -> float:
    """Euclidian distance between two RGB colors."""

    return sum(map(lambda x: (x[0] - x[1])**2, zip(a[0:3], b[0:3])))

def color_dist_redmean(a: RGBA | RGB, b: RGBA | RGB) -> float:
    """Somewhat sophisticated color distance function."""

    dr = a[0] - b[0]
    dg = a[1] - b[1]
    db = a[2] - b[2]
    R = dr/2
    return (2 + R/255)*dr**2 + 4*dg**2 + (2 + (255 - R)/255)*db**2

class MapColor(NamedTuple):
    """Container for a complete color specification.

    'id' is the base color.
    'rgba' is the actual RGB values of the color.
    'shade' is the shade of the color.
    """

    id: BaseColor
    rgba: RGBA
    shade: ColorShade = ColorShade.BASE

    def from_game_id(game_id: int) -> Self:
        """Returns a 'MapColor' from an internal Minecraft color id (used for maps)."""

        return base_colors[game_id // 4].reshade(game_id % 4)

    def from_rgba(rgba: RGBA, dist: RGBADist = color_dist_redmean, colors: list[Self] | None = None) -> Self:
        """Returns a 'MapColor' corresponding to the provided RGB values.

        'dist' is a color distance function to determine the closest matching
        color. When 'None', raises a 'ValueError' on a non-existant color.
        'colors' is the list of colors to use. When 'None', defaults to
        'complete_colors_vanilla' without NONE.
        """
        # ignores NONE colors
        if colors == None: colors = complete_colors_vanilla[3:]

        if len(rgba) == 3: rgba = (*rgba, 255)

        if dist == None:
            try:
                return next(filter(lambda x: x.rgba == rgba, colors))
            except StopIteration:
                raise ValueError(f"Invalid color: {rgba}")

        return sorted(((dist(rgba, col.rgba), col) for col in colors), key = lambda x: x[0])[0][1]

    def game_id(self: Self) -> int:
        """Returns the internal Minecraft map color id."""

        return int(self.id*4 + self.shade)

    def reshade(self: Self, shade: ColorShade, allow_non_base: bool = False) -> Self:
        """Calculates and returns a 'MapColor' for the given shade.

        'shade' is the 'ColorShade' to generate.
        'allow_non_base' enables shading non-base colors.

        Raises a 'ValueError' when trying to calculate a shade of a non-base
        color (unless 'allow_non_base' is 'True').
        Also raises a 'ValueError' when given an invalid 'ColorShade'.
        """

        if not allow_non_base and self.shade != ColorShade.BASE:
            raise ValueError("Trying to shade a non-base color.")
        if shade not in shade_multipliers:
            raise ValueError("Trying to shade to an invalid ColorShade.")

        m = shade_multipliers[shade]
        return MapColor(id=self.id, rgba=(
                (self.rgba[0] * m) // 255,
                (self.rgba[1] * m) // 255,
                (self.rgba[2] * m) // 255,
                self.rgba[3],
            ), shade=shade)

base_colors: list[MapColor] = [
    MapColor(id=BaseColor.NONE, rgba=(0, 0, 0, 0)),
    MapColor(id=BaseColor.GRASS, rgba=(127, 178, 56, 255)),
    MapColor(id=BaseColor.SAND, rgba=(247, 233, 163, 255)),
    MapColor(id=BaseColor.WOOL, rgba=(199, 199, 199, 255)),
    MapColor(id=BaseColor.FIRE, rgba=(255, 0, 0, 255)),
    MapColor(id=BaseColor.ICE, rgba=(160, 160, 255, 255)),
    MapColor(id=BaseColor.METAL, rgba=(167, 167, 167, 255)),
    MapColor(id=BaseColor.PLANT, rgba=(0, 124, 0, 255)),
    MapColor(id=BaseColor.SNOW, rgba=(255, 255, 255, 255)),
    MapColor(id=BaseColor.CLAY, rgba=(164, 168, 184, 255)),
    MapColor(id=BaseColor.DIRT, rgba=(151, 109, 77, 255)),
    MapColor(id=BaseColor.STONE, rgba=(112, 112, 112, 255)),
    MapColor(id=BaseColor.WATER, rgba=(64, 64, 255, 255)),
    MapColor(id=BaseColor.WOOD, rgba=(143, 119, 72, 255)),
    MapColor(id=BaseColor.QUARTZ, rgba=(255, 252, 245, 255)),
    MapColor(id=BaseColor.COLOR_ORANGE, rgba=(216, 127, 51, 255)),
    MapColor(id=BaseColor.COLOR_MAGENTA, rgba=(178, 76, 216, 255)),
    MapColor(id=BaseColor.COLOR_LIGHT_BLUE, rgba=(102, 153, 216, 255)),
    MapColor(id=BaseColor.COLOR_YELLOW, rgba=(229, 229, 51, 255)),
    MapColor(id=BaseColor.COLOR_LIGHT_GREEN, rgba=(127, 204, 25, 255)),
    MapColor(id=BaseColor.COLOR_PINK, rgba=(242, 127, 165, 255)),
    MapColor(id=BaseColor.COLOR_GRAY, rgba=(76, 76, 76, 255)),
    MapColor(id=BaseColor.COLOR_LIGHT_GRAY, rgba=(153, 153, 153, 255)),
    MapColor(id=BaseColor.COLOR_CYAN, rgba=(76, 127, 153, 255)),
    MapColor(id=BaseColor.COLOR_PURPLE, rgba=(127, 63, 178, 255)),
    MapColor(id=BaseColor.COLOR_BLUE, rgba=(51, 76, 178, 255)),
    MapColor(id=BaseColor.COLOR_BROWN, rgba=(102, 76, 51, 255)),
    MapColor(id=BaseColor.COLOR_GREEN, rgba=(102, 127, 51, 255)),
    MapColor(id=BaseColor.COLOR_RED, rgba=(153, 51, 51, 255)),
    MapColor(id=BaseColor.COLOR_BLACK, rgba=(25, 25, 25, 255)),
    MapColor(id=BaseColor.GOLD, rgba=(250, 238, 77, 255)),
    MapColor(id=BaseColor.DIAMOND, rgba=(92, 219, 213, 255)),
    MapColor(id=BaseColor.LAPIS, rgba=(74, 128, 255, 255)),
    MapColor(id=BaseColor.EMERALD, rgba=(0, 217, 58, 255)),
    MapColor(id=BaseColor.PODZOL, rgba=(129, 86, 49, 255)),
    MapColor(id=BaseColor.NETHER, rgba=(112, 2, 0, 255)),
    MapColor(id=BaseColor.TERRACOTTA_WHITE, rgba=(209, 177, 161, 255)),
    MapColor(id=BaseColor.TERRACOTTA_ORANGE, rgba=(159, 82, 36, 255)),
    MapColor(id=BaseColor.TERRACOTTA_MAGENTA, rgba=(149, 87, 108, 255)),
    MapColor(id=BaseColor.TERRACOTTA_LIGHT_BLUE, rgba=(112, 108, 138, 255)),
    MapColor(id=BaseColor.TERRACOTTA_YELLOW, rgba=(186, 133, 36, 255)),
    MapColor(id=BaseColor.TERRACOTTA_LIGHT_GREEN, rgba=(103, 117, 53, 255)),
    MapColor(id=BaseColor.TERRACOTTA_PINK, rgba=(160, 77, 78, 255)),
    MapColor(id=BaseColor.TERRACOTTA_GRAY, rgba=(57, 41, 35, 255)),
    MapColor(id=BaseColor.TERRACOTTA_LIGHT_GRAY, rgba=(135, 107, 98, 255)),
    MapColor(id=BaseColor.TERRACOTTA_CYAN, rgba=(87, 92, 92, 255)),
    MapColor(id=BaseColor.TERRACOTTA_PURPLE, rgba=(122, 73, 88, 255)),
    MapColor(id=BaseColor.TERRACOTTA_BLUE, rgba=(76, 62, 92, 255)),
    MapColor(id=BaseColor.TERRACOTTA_BROWN, rgba=(76, 50, 35, 255)),
    MapColor(id=BaseColor.TERRACOTTA_GREEN, rgba=(76, 82, 42, 255)),
    MapColor(id=BaseColor.TERRACOTTA_RED, rgba=(142, 60, 46, 255)),
    MapColor(id=BaseColor.TERRACOTTA_BLACK, rgba=(37, 22, 16, 255)),
    MapColor(id=BaseColor.CRIMSON_NYLIUM, rgba=(189, 48, 49, 255)),
    MapColor(id=BaseColor.CRIMSON_STEM, rgba=(148, 63, 97, 255)),
    MapColor(id=BaseColor.CRIMSON_HYPHAE, rgba=(92, 25, 29, 255)),
    MapColor(id=BaseColor.WARPED_NYLIUM, rgba=(22, 126, 134, 255)),
    MapColor(id=BaseColor.WARPED_STEM, rgba=(58, 142, 140, 255)),
    MapColor(id=BaseColor.WARPED_HYPHAE, rgba=(86, 44, 62, 255)),
    MapColor(id=BaseColor.WARPED_WART_BLOCK, rgba=(20, 180, 133, 255)),
    MapColor(id=BaseColor.DEEPSLATE, rgba=(100, 100, 100, 255)),
    MapColor(id=BaseColor.RAW_IRON, rgba=(216, 175, 147, 255)),
    MapColor(id=BaseColor.GLOW_LICHEN, rgba=(127, 167, 150, 255)),
]

def generate_complete_colors(include_impossible: bool = True) -> list[MapColor]:
    """Generates all possible colors and shades.

    'include_impossible' controls whether to include 'ColorShade.DARK', as it
    is impossible to get in-game.
    """

    out = []
    for color in base_colors:
        out.append(color.reshade(ColorShade.SLOPE_NS))
        out.append(color.reshade(ColorShade.PLANE))
        out.append(color.reshade(ColorShade.SLOPE_SN))
        if include_impossible:
            out.append(color.reshade(ColorShade.DARK))
    return out

complete_colors = generate_complete_colors(include_impossible = True)
complete_colors_vanilla = generate_complete_colors(include_impossible = False)

def color_array_to_gimp_palette(colors: list[MapColor], out_path: str = "Minecraft-Map.gpl", name: str = "Minecraft Map", columns: int = 16, vertical: bool = True) -> bool:
    """Generate a GIMP color palette from a list of colors.

    'colors' is a list of all 'MapColor's to be included.
    'out_path' is the path to the palette.
    'name' is the name of the palette, visible in GIMP.
    'columns' is the amount of columns, or the width, of the palette.
    'vertical' controls whether to align shades of the same base color
    vertically.
    """

    if len(colors) == 0:
        return False

    shades = []
    unique_shades = list(set(x.shade for x in colors))
    if vertical:
        for shade in unique_shades:
            shades.append([ x for x in colors if x.shade == shade ])

        if not all(len(x) == len(shades[0]) for x in shades):
            raise ValueError("Cannot vertically align colors with a different amount of shades.")
    else:
        shades.append(colors)

    columns = min(columns, len(shades[0]))
    if not vertical:
        columns -= columns % len(unique_shades)

    with open(out_path, "w") as f:
        f.write(f"GIMP Palette\nName: {name}\nColumns: {columns}\n#\n")
        for i in range(0, len(shades[0]), columns):
            cols = []
            for arr in shades:
                row = arr[i:i+columns]
                if vertical:
                    row += [MapColor(id=BaseColor.NONE, rgba=(0,0,0,0))] * (columns - len(row))
                cols += row

            for color in cols:
                f.write(f"{color.rgba[0]:>3d} {color.rgba[1]:>3d} {color.rgba[2]:>3d}\t{str(color.id)}_{color.shade}\n")

    return True

def array_to_columns(array: list, width: int) -> list[list]:
    """Turns a one-dimensional array to a column-major two dimensional array."""

    return [ array[i::width] for i in range(width) ]

def columns_to_array(columns: list[list]) -> tuple[list, int]:
    """Turns a column-major two dimensional array to a one-dimensional array."""

    return list(reduce(lambda x,y: x+y, zip(*columns))), len(columns[0])

def rgba_array_to_map_color(array: list[RGBA | RGB], dist: RGBADist = color_dist_redmean, colors: list[MapColor] | None = None) -> list[MapColor]:
    """Converts a list of RGBA tuples to a list of 'MapColor'.

    'dist' is a color distance function to determine the closest matching
    color. When 'None', raises a 'ValueError' on a non-existant color.
    'colors' contains all possible colors to choose from. When 'None', defaults
    to 'complete_colors_vanilla' without NONE.
    """

    return [ MapColor.from_rgba(x, dist=dist, colors=colors) for x in array ]

# TODO: be smarter with height (using bigger slopes to reduce overall size)
def rgba_image_to_height_map(image: list[RGBA | RGB], width: int, dist: RGBADist = color_dist_redmean, colors: list[MapColor] | None = None) -> list[list[tuple[BaseColor, int, int]]]:
    """Converts an RGBA image to a height map of base colors which produces the desired map in-game.

    'image' is a list of colors in row-major order.
    'width' is the width of the image.
    'dist' is a color distance function to determine the closest matching
    color. When 'None', raises a 'ValueError' on a non-existant color.
    'colors' contains all possible colors to choose from. When 'None', defaults
    to 'complete_colors_vanilla' without NONE.

    Returns a list of all blocks in the image, from north to south. All entries
    consistsa of the 'BaseColor', a possible height (offset from the first
    block of the column), and the change from the last block. The height/change
    value for the first row indicates how they must be placed in relation to
    the blocks outside the image to produce the desired color.
    """

    cols = array_to_columns(rgba_array_to_map_color(image, dist=dist, colors=colors), width)

    out_cols = []
    for col in cols:
        current_col = []
        # columns go from N to S
        prev_h = 0
        for color in col:
            match color.shade:
                case ColorShade.SLOPE_NS:
                    prev_h -= 1
                    change = -1
                case ColorShade.SLOPE_SN:
                    prev_h += 1
                    change = 1
                case _: # ColorShade.PLANE or invalid
                    prev_h = prev_h
                    change = 0

            current_col.append((color.id, prev_h, change))

        out_cols.append(current_col)

    return columns_to_array(out_cols)[0]
