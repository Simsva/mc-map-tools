#!/usr/bin/env python3

"""
Generate a new image with corrected colors to match the in-game colors.
"""

from mcmap import *
from PIL import Image

if len(sys.argv) < 3:
    in_path = input("In path: ")
    out_path = input("Out path: ")
else:
    in_path = sys.argv[1]
    out_path = sys.argv[2]
valid_colors = complete_colors_vanilla
if len(sys.argv) > 3 and sys.argv[3] == "all":
    valid_colors = complete_colors

try:
    img = Image.open(in_path)
except FileNotFoundError:
    print(f"File not found: '{in_path}'")
    exit(1)

colors = rgba_array_to_map_color(list(img.getdata()), colors=valid_colors)
img.close()

new_img = Image.new("RGB", (img.width, img.height))
pixels = new_img.load()
for x in range(img.width):
    for y in range(img.height):
        new_img.putpixel((x, y), colors[x + img.width*y].rgba[:3])

new_img.save(out_path, "PNG")
