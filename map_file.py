#!/usr/bin/env python3

"""
Generate a Minecraft map.dat file from a 128x128 image
"""

from mcmap import *
from PIL import Image
import nbtlib
from nbtlib.tag import Byte, String, Int, List, Compound, ByteArray

MapFile = nbtlib.schema("MapFile", {
    "data": Compound,
    "DataVersion": Int,
})

MapData = nbtlib.schema("MapData", {
    "scale": Byte,
    "dimension": String,
    "trackingPosition": Byte,
    "unlimitedTracking": Byte,
    "locked": Byte,
    "xCenter": Int,
    "zCenter": Int,
    "banners": List,
    "frames": List,
    "colors": ByteArray,
})

if len(sys.argv) < 3:
    in_path = input("In path: ")
    out_path = input("Out path: ")
else:
    in_path = sys.argv[1]
    out_path = sys.argv[2]

try:
    img = Image.open(in_path)
except FileNotFoundError:
    print(f"File not found: '{in_path}'")
    exit(1)

assert img.width == 128
assert img.height == 128

colors = rgba_array_to_map_color(list(img.getdata()), dist = None, colors = complete_colors)
img.close()

data = MapData({
    "scale": 0,
    "dimension": "minecraft:overworld",
    "trackingPosition": 0,
    "unlimitedTracking": 0,
    "locked": 1,
    "xCenter": 0,
    "zCenter": 0,
    "banners": [],
    "frames": [],
    # disgusting
    "colors": [ x.game_id() for x in colors ],
})

map_file = MapFile({
    "DataVersion": 3700, # 1.20.4
    "data": data,
})
nbtlib.nbt.File(map_file).save(out_path, gzipped=True, byteorder="big")
