#!/usr/bin/env python3

"""
Generate a litematic of an image. Expects only valid colors, so run
`generate_image.py` first.
"""

from mcmap import *
from litemapy import Region, BlockState
from PIL import Image

# filler block
filler_block = BlockState("minecraft:stone")

# block palette in order of BaseColor
palette = [
    BlockState("minecraft:air"),
    BlockState("minecraft:slime_block"),
    BlockState("minecraft:sand"),
    BlockState("minecraft:cobweb"),
    BlockState("minecraft:redstone_block"),
    BlockState("minecraft:ice"),
    BlockState("minecraft:iron_block"),
    BlockState("minecraft:oak_leaves"),
    BlockState("minecraft:white_carpet"),
    BlockState("minecraft:clay"),
    BlockState("minecraft:dirt"),
    BlockState("minecraft:stone"),
    BlockState("minecraft:water"),
    BlockState("minecraft:oak_planks"),
    BlockState("minecraft:diorite"),
    BlockState("minecraft:orange_carpet"),
    BlockState("minecraft:magenta_carpet"),
    BlockState("minecraft:light_blue_carpet"),
    BlockState("minecraft:yellow_carpet"),
    BlockState("minecraft:lime_carpet"),
    BlockState("minecraft:pink_carpet"),
    BlockState("minecraft:gray_carpet"),
    BlockState("minecraft:light_gray_carpet"),
    BlockState("minecraft:cyan_carpet"),
    BlockState("minecraft:purple_carpet"),
    BlockState("minecraft:blue_carpet"),
    BlockState("minecraft:brown_carpet"),
    BlockState("minecraft:green_carpet"),
    BlockState("minecraft:red_carpet"),
    BlockState("minecraft:black_carpet"),
    BlockState("minecraft:gold_block"),
    BlockState("minecraft:diamond_block"),
    BlockState("minecraft:lapis_block"),
    BlockState("minecraft:emerald_block"),
    BlockState("minecraft:spruce_planks"),
    BlockState("minecraft:netherrack"),
    BlockState("minecraft:white_terracotta"),
    BlockState("minecraft:orange_terracotta"),
    BlockState("minecraft:magenta_terracotta"),
    BlockState("minecraft:light_blue_terracotta"),
    BlockState("minecraft:yellow_terracotta"),
    BlockState("minecraft:lime_terracotta"),
    BlockState("minecraft:pink_terracotta"),
    BlockState("minecraft:gray_terracotta"),
    BlockState("minecraft:light_gray_terracotta"),
    BlockState("minecraft:cyan_terracotta"),
    BlockState("minecraft:purple_terracotta"),
    BlockState("minecraft:blue_terracotta"),
    BlockState("minecraft:brown_terracotta"),
    BlockState("minecraft:green_terracotta"),
    BlockState("minecraft:red_terracotta"),
    BlockState("minecraft:black_terracotta"),
    BlockState("minecraft:crimson_nylium"),
    BlockState("minecraft:crimson_planks"),
    BlockState("minecraft:crimson_hyphae"),
    BlockState("minecraft:warped_nylium"),
    BlockState("minecraft:warped_planks"),
    BlockState("minecraft:warped_hyphae"),
    BlockState("minecraft:warped_wart_block"),
    BlockState("minecraft:deepslate"),
    BlockState("minecraft:raw_iron_block"),
    BlockState("minecraft:verdant_froglight"),
]

if len(sys.argv) < 3:
    in_path = input("In path: ")
    out_path = input("Out path: ")
else:
    in_path = sys.argv[1]
    out_path = sys.argv[2]

try:
    with Image.open(in_path) as img:
        height_map = rgba_image_to_height_map(list(img.getdata()), img.width, dist=None)
        # leave room for a filler northernmost row
        sz_x, sz_z = img.width, img.height + 1
except FileNotFoundError:
    print(f"File not found: '{in_path}'")
    exit(1)

min_y = min(( x[1] for x in height_map ))
max_y = max(( x[1] for x in height_map ))
sz_y = max_y - min_y + 1

reg = Region(0,0,0, sz_x,sz_y,sz_z)
schem = reg.as_schematic(name=in_path, author="mcmap.py", description="Map art")

for i, block in enumerate(height_map):
    x, z = i % sz_x, i // sz_x + 1
    y = block[1] - min_y
    val = int(block[0])

    print(f"x:{x} y:{y} z:{z} block:{palette[val]}")
    reg.setblock(x, y, z, palette[val])
    if z == 1:
        # Add in a filler row to get the correct shade on the northernmost row
        reg.setblock(x, y-block[2], z-1, filler_block)

print("Saving...")
schem.save(out_path)
