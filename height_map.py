#!/usr/bin/env python3

"""
Generate a "height map" from an image.
"""

from mcmap import *
from PIL import Image

if len(sys.argv) < 2:
    path = input("Image path: ")
else:
    path = sys.argv[1]

try:
    img = Image.open(path)
except FileNotFoundError:
    print(f"File not found: '{path}'")
    exit(1)

chars = ('0', '+', '-')
height_map = array_to_columns(rgba_image_to_height_map(list(img.getdata()), img.width), img.width)
for col in height_map:
    print(" ".join(f"{str(x[0])}:{chars[x[2]]}" for x in col))
